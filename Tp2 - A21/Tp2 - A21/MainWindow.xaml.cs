﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tp2___A21
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Attributs

        private MoteurDeJeu _leJeu;
        private int _carteSelectionnee = -1;
        private bool _estConnecte;
        private int _nbJoueurs;
        private Dictionary<string, Joueur> _dicoJoueurs;
        private Dictionary<string, byte[]> _dicoSalts;

        #endregion

        #region Accesseurs

        public bool EstConnecte
        {
            get { return _estConnecte; }
            set { _estConnecte = value; }
        }

        public int NbJoueurs
        {
            get { return _nbJoueurs; }
            set { _nbJoueurs = value; }
        }

        #endregion

        /// <summary>
        /// Au moment d'initialiser la fenêtre, on dessiner tous les objets, on charge les données et on initialise les messages.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            ChargerUtilisateurs();
            ChargerSalts();
            DessinerObjetsNecessitentConnexion(EstConnecte);
            txtIdentifiant.Focus();
            lblMessage.Content = "";
            lblMessage2.Content = "";
        }

        /// <summary>
        /// Cette méthode permet de charger les utilisateurs à partir d'un fichier JSON.
        /// </summary>
        private void ChargerUtilisateurs()
        {
            if (File.Exists("users.json"))
            {
                _dicoJoueurs = (Dictionary<string, Joueur>)JsonSerializer.Deserialize(
                    File.ReadAllText("users.json"), typeof(Dictionary<string, Joueur>));

            }
            else
            {
                _dicoJoueurs = new Dictionary<string, Joueur>();
            }
        }

        /// <summary>
        /// Cette méthode permet de charger les salts à partir d'un fichier JSON.
        /// </summary>
        private void ChargerSalts()
        {
            if (File.Exists("salts.json"))
            {
                _dicoSalts = (Dictionary<string, byte[]>)JsonSerializer.Deserialize(
                    File.ReadAllText("salts.json"), typeof(Dictionary<string, byte[]>));
            }
            else
            {
                _dicoSalts = new Dictionary<string, byte[]>();
            }
        }

        private void Window_Closing(object pSender, System.ComponentModel.CancelEventArgs pE)
        {
            JsonSerializerOptions options = new JsonSerializerOptions { WriteIndented = true };
            File.WriteAllText("users.json", JsonSerializer.Serialize(_dicoJoueurs, typeof(Dictionary<string, Joueur>), options));


            File.WriteAllText("salts.json", JsonSerializer.Serialize(_dicoSalts, typeof(Dictionary<string, byte[]>), options));
        }

        /// <summary>
        /// Cette méthode dessine les cartes, ainsi que plusieurs labels et messages.
        /// </summary>
        private void Dessiner()
        {
            DessinerPaquetEtDefausse();
            DessinerJoueurs();
            lblJoueur.Visibility = Visibility.Visible;
            for (int i = 0; i < NbJoueurs - 1; i++)
            {
                ((Label)maGrid.FindName("lblBot" + (i + 1).ToString())).Visibility = Visibility.Visible;
                lblMessage.Content = "";
            }

            if (_leJeu.CartePouvoir8 is not null)
            {
                lblMessage.Content = $"La couleur actuelle est {_leJeu.CartePouvoir8.SorteCarte}.";
            }
        }

        /// <summary>
        /// Cette méthode dessine les labels des joueurs selon le nombre de joueurs.
        /// </summary>
        /// <param name="pEstConnecte">Si l'utilisateur est connecté.</param>
        private void DessinerObjetsNecessitentConnexion(bool pEstConnecte)
        {
            lbl24.Visibility = pEstConnecte ? Visibility.Visible : Visibility.Hidden;
            lblNbJoueurs.Visibility = pEstConnecte ? Visibility.Visible : Visibility.Hidden;
            txtNbJoueurs.Visibility = pEstConnecte ? Visibility.Visible : Visibility.Hidden;
            btnJouer.Visibility = pEstConnecte ? Visibility.Visible : Visibility.Hidden;
            lblJoueur.Visibility = pEstConnecte ? Visibility.Visible : Visibility.Hidden;
            if (!pEstConnecte)
            {
                lblBot1.Visibility = Visibility.Hidden;
                lblBot2.Visibility = Visibility.Hidden;
                lblBot3.Visibility = Visibility.Hidden;

            }
        }

        /// <summary>
        /// Permet de dessiner le paquet et la defausse selon la carte du dessus.
        /// </summary>
        private void DessinerPaquetEtDefausse()
        {
            cnvDefausse.Children.Clear();
            cnvPaquet.Children.Clear();
            if (_leJeu.DefausseVide() == false)
            {
                Image defausse = new Image();
                _ = _leJeu.ObtenirSommetDefausse();
                defausse.Source =
                    BitmapFrame.Create(new Uri(
                        "Cartes/" + _leJeu.ObtenirSommetDefausse().ObtenirNomFichier(), UriKind.Relative));
                defausse.Width = 72;
                defausse.Height = 96;
                cnvDefausse.Children.Add(defausse);
            }
            else
            {
                Rectangle carteBlanche = new()
                {
                    Height = 96,
                    Width = 72,
                    Fill = new SolidColorBrush(Colors.White)
                };
                cnvDefausse.Children.Add(carteBlanche);
            }

            if (_leJeu.PaquetVide() == false)
            {
                Image paquet = new Image
                {
                    Source = BitmapFrame.Create(new Uri("Cartes/b1fv.png", UriKind.Relative)),
                    Width = 72,
                    Height = 96
                };
                cnvPaquet.Children.Add(paquet);
            }
            else
            {
                Rectangle carteBlanche = new Rectangle
                {
                    Height = 96,
                    Width = 72,
                    Fill = new SolidColorBrush(Colors.White)
                };
                cnvPaquet.Children.Add(carteBlanche);
            }
        }

        /// <summary>
        /// Dessine les cartes des joueurs.
        /// </summary>
        private void DessinerJoueurs()
        {
            int decalageSelection = -8;
            int decalageCarte = 0;

            for (int i = 0; i < NbJoueurs; i++)
            {
                Joueur monJoueurActuel = _leJeu.LesJoueurs.Dequeue();
                ((Canvas)maGrid.FindName("cnvJoueur" + (i + 1).ToString())).Children.Clear();
                for (int j = 0; j < monJoueurActuel.Main.Count; j++)
                {
                    monJoueurActuel.Main = _leJeu.OrdonnerCartes(monJoueurActuel.Main);
                    List<Carte> lstTempo = monJoueurActuel.Main.ToList();
                    Carte carteActuelle = lstTempo[j];
                    Image monImage = new Image
                    {
                        Source =
                            BitmapFrame.Create(new Uri("Cartes/" + carteActuelle.ObtenirNomFichier(),
                                UriKind.Relative)),
                        Width = 72,
                        Height = 96
                    };
                    if (i == 0 && j == _carteSelectionnee)
                    {
                        Canvas.SetTop(monImage, decalageSelection);
                    }
                    Canvas.SetLeft(monImage, decalageCarte);
                    ((Canvas)maGrid.FindName("cnvJoueur" + (i + 1).ToString())).Children.Add(monImage);
                    decalageCarte += 14;


                    CacherMain(monJoueurActuel, monImage);
                }

                decalageCarte = 0;
                _leJeu.LesJoueurs.Enqueue(monJoueurActuel);

            }
        }

        /// <summary>
        /// Methode qui cache la main des joueurs automatisé.
        /// </summary>
        /// <param name="pJoueur">Le joueur qui aura sa main cachée.</param>
        /// <param name="pImage">L'image qui changera.</param>
        private void CacherMain(Joueur pJoueur, Image pImage)
        {
            if (pJoueur is JoueurAutomatise)
            {
                pImage.Source = BitmapFrame.Create(new Uri("Cartes/b1fv.png", UriKind.Relative));
            }
        }


        private void cnvJoueur1_MouseUp(object pSender, MouseButtonEventArgs pE)
        {
            Point leClick = pE.GetPosition(cnvJoueur1);
            _carteSelectionnee = leClick.X < (82 + 14 * _leJeu.LesJoueurs.Peek().Main.Count()) && leClick.Y > 8
                ? Math.Min(((int)leClick.X) / 14, _leJeu.LesJoueurs.Peek().Main.Count - 1)
                : -1;
            Dessiner();
        }

        /// <summary>
        /// Cette méthode gère la fin de partie et le moteur de jeu.
        /// </summary>
        private void FaireUnTour()
        {
            string gagnant = _leJeu.FaireUnTour();

            Dessiner();

            if (gagnant.Length != 0)
            {
                FinPartie(gagnant);
            }

        }

        /// <summary>
        /// Permet de terminer la partie.
        /// </summary>
        /// <param name="pGagnant">Le gagnant de la partie.</param>
        private void FinPartie(string pGagnant)
        {
            for (int i = 0; i < _leJeu.NbJoueurs; i++)
            {
                ((Canvas)maGrid.FindName(name: $"cnvJoueur{i + 1}"))?.Children.Clear();
            }
            ((Canvas)maGrid.FindName(name: "cnvDefausse"))?.Children.Clear();
            ((Canvas)maGrid.FindName(name: "cnvPaquet"))?.Children.Clear();

            lblMessage.Content = $"Le joueur: {pGagnant} a gagné la partie.";

            _leJeu = null;
        }


        private void cnvPaquet_MouseUp(object pSender, MouseButtonEventArgs pE)
        {
            _leJeu.PigerCarte(_leJeu.LesJoueurs.Peek());
            FaireUnTour();
        }

        private void cnvDefausse_MouseUp(object pSender, MouseButtonEventArgs pE)
        {
            if (_carteSelectionnee > -1)
            {
                List<Carte> lstTempo = _leJeu.LesJoueurs.Peek().Main.ToList();
                Carte carte = lstTempo[_carteSelectionnee];

                Carte carteAJouer = _leJeu.CartePouvoir8?.Valeur == 8 ? _leJeu.CartePouvoir8 : _leJeu.ObtenirSommetDefausse();


                if (!carte.JouerAnytime && carteAJouer.Valeur != carte.Valeur &&
                    carteAJouer.SorteCarte != carte.SorteCarte)
                {
                    lblMessage.Content =
                        "La carte sélectionnée doit être de la même sorte ou valeur que le sommet de la défausse.";
                    lblMessage.Foreground = Brushes.Red;
                }
                else
                {
                    lblMessage.Foreground = Brushes.White;
                    string gagnant = _leJeu.JouerCarteHumain(carte);
                    if (gagnant.Length != 0)
                    {
                        FinPartie(gagnant);
                    }
                    else
                    {
                        _carteSelectionnee = -1;
                        FaireUnTour();
                    }
                }
            }
        }

        #region Style visuel

        private void btnInscription_MouseEnter(object pSender, MouseEventArgs pE)
        {
            btnInscription.Foreground = Brushes.Black;
        }

        private void btnInscription_MouseLeave(object pSender, MouseEventArgs pE)
        {
            btnInscription.Foreground = Brushes.White;
        }
        private void btnInscription_GotFocus(object pSender, RoutedEventArgs pE)
        {
            btnInscription.BorderBrush = Brushes.White;
        }
        private void btnInscription_LostFocus(object pSender, RoutedEventArgs pE)
        {
            btnInscription.BorderBrush = (Brush)new BrushConverter().ConvertFromString("#FF0095B9");
        }

        private void btnConnexion_MouseEnter(object pSender, MouseEventArgs pE)
        {
            btnConnexion.Foreground = Brushes.Black;
        }

        private void btnConnexion_MouseLeave(object pSender, MouseEventArgs pE)
        {
            btnConnexion.Foreground = Brushes.White;
        }
        private void btnConnexion_GotFocus(object pSender, RoutedEventArgs pE)
        {
            btnConnexion.BorderBrush = Brushes.White;
        }
        private void btnConnexion_LostFocus(object pSender, RoutedEventArgs pE)
        {
            btnConnexion.BorderBrush = (Brush)new BrushConverter().ConvertFromString("#FF0095B9");
        }

        private void btnJouer_MouseEnter(object pSender, MouseEventArgs pE)
        {
            btnJouer.Foreground = Brushes.Black;
        }

        private void btnJouer_MouseLeave(object pSender, MouseEventArgs pE)
        {
            btnJouer.Foreground = Brushes.White;
        }

        private void btnJouer_GotFocus(object pSender, RoutedEventArgs pE)
        {
            btnJouer.BorderBrush = Brushes.White;
        }

        private void btnJouer_LostFocus(object pSender, RoutedEventArgs pE)
        {
            btnJouer.BorderBrush = (Brush)new BrushConverter().ConvertFromString("#FF0095B9");
        }

        #endregion

        private void btnConnexion_Click(object pSender, RoutedEventArgs pE)
        {
            if (_dicoJoueurs.ContainsKey(txtIdentifiant.Text))
            {
                if (Utilitaires.VerifierMdp(txtPassword.Password, _dicoSalts[txtIdentifiant.Text],
                    _dicoJoueurs[txtIdentifiant.Text].Mdp))
                {
                    lblMessage2.Content =
                        $"Bienvenu {_dicoJoueurs[txtIdentifiant.Text].Nom} ! Vous êtes connecté.";
                    lblMessage2.Foreground = Brushes.White;
                    EstConnecte = true;
                    DessinerObjetsNecessitentConnexion(EstConnecte);

                    txtNbJoueurs.Focus();
                    btnConnexion.IsDefault = false;
                    btnJouer.IsDefault = true;
                }
                else
                {
                    lblMessage2.Content = "Mot de passe incorrect.";
                    lblMessage2.Foreground = Brushes.Red;
                    txtIdentifiant.Focus();
                }
            }

            else
            {
                lblMessage2.Content = "Identifiant non-existant.";
                lblMessage2.Foreground = Brushes.Red;
            }

            btnJouer.Focus();
        }

        private void btnJouer_Click(object pSender, RoutedEventArgs pE)
        {
            if (!int.TryParse(txtNbJoueurs.Text, out _))
            {
                txtNbJoueurs.Foreground = Brushes.Red;
                lblMessage2.Content = "Veuillez entrer un nombre entier.";
                lblMessage2.Foreground = Brushes.Red;
                txtNbJoueurs.Foreground = Brushes.White;
                txtNbJoueurs.Clear();
                txtNbJoueurs.Focus();
            }
            else
            {
                NbJoueurs = Convert.ToInt32(txtNbJoueurs.Text);
                if (NbJoueurs < 2 || NbJoueurs > 4)
                {
                    txtNbJoueurs.Foreground = Brushes.Red;
                    lblMessage2.Content = "Le nombre de joueurs doit être 2, 3 ou 4.";
                    lblMessage2.Foreground = Brushes.Red;
                    txtNbJoueurs.Foreground = Brushes.White;
                    txtNbJoueurs.Clear();
                    txtNbJoueurs.Focus();
                }
                else
                {
                    _leJeu = new MoteurDeJeu(NbJoueurs, txtIdentifiant.Text);
                    lblMessage2.Foreground = Brushes.White;
                    lblMessage2.Content = "";
                    Effacer();
                    Dessiner();
                    lblJoueur.Content = txtIdentifiant.Text;
                }
            }

        }

        /// <summary>
        /// Cette méthode efface les joueurs ne faisant pas partie du jeu.
        /// </summary>
        private void Effacer()
        {
            for (int i = 0; i < NbJoueurs; i++)
            {
                switch (i)
                {
                    case 1:
                        cnvJoueur3.Children.Clear();
                        lblBot2.Visibility = Visibility.Hidden;
                        cnvJoueur4.Children.Clear();
                        lblBot3.Visibility = Visibility.Hidden;
                        break;
                    case 2:
                        cnvJoueur4.Children.Clear();
                        lblBot3.Visibility = Visibility.Hidden;
                        break;
                }
            }
        }

        private void btnInscription_Click(object pSender, RoutedEventArgs pE)
        {
            if (_dicoJoueurs.ContainsKey(txtIdentifiant.Text))
            {

                lblMessage2.Content = "Identifiant déjà existant.";
                lblMessage2.Foreground = Brushes.Red;
                btnConnexion.Focus();
            }
            else
            {
                _dicoSalts.Add(txtIdentifiant.Text, Utilitaires.SaltMotDePasse());
                Joueur user = new Joueur(txtIdentifiant.Text,
                    Utilitaires.HashMotDePasse(txtPassword.Password, _dicoSalts[txtIdentifiant.Text]));
                _dicoJoueurs.Add(txtIdentifiant.Text, user);
                lblMessage2.Content = "Création d'un compte est un succès.";
                lblMessage2.Foreground = Brushes.White;
                btnConnexion_Click(pSender, pE);
                txtNbJoueurs.Focus();
                btnConnexion.IsDefault = false;
                btnJouer.IsDefault = true;
            }
        }

    }
}