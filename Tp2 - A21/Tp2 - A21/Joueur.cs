﻿using System.Collections.Generic;

namespace Tp2___A21
{
    public class Joueur
    {
        private LinkedList<Carte> _main;
        private string _nom;
        private byte[] _mdp;

        private int _nbPige;

        public int NbPige
        {
            get { return _nbPige; }
            set { _nbPige = value; }
        }

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public LinkedList<Carte> Main
        {
            get { return _main; }
            set { _main = value; }
        }

        public byte[] Mdp
        {
            get { return _mdp; }
            set { _mdp = value; }
        }

        /// <summary>
        /// Constructeur qui initialise le nom et le mot de passe du joueur.
        /// </summary>
        /// <param name="pNom">Le nom du joueur.</param>
        /// <param name="pMdp">Le mot de passe du joueur.</param>
        public Joueur(string pNom, byte[] pMdp)
        {
            Nom = pNom;
            Mdp = pMdp;
        }

        /// <summary>
        /// Constructeur qui sert à créer un joueur utilisé dans le
        /// jeu ayant une main de cartes, un nom et le nombre de cartes pigées.
        /// </summary>
        /// <param name="pNom">Le nom du joueur.</param>
        public Joueur(string pNom)
        {
            Main = new LinkedList<Carte>();
            Nom = pNom;
            _nbPige = 1;
        }

        public Joueur()
        {

        }

        /// <summary>
        /// Permet d'obtenir la sorte de carte désirée par le joueur.
        /// </summary>
        /// <returns>Retourne la sorte choisie.</returns>
        public virtual Carte.Sorte ObtenirSortePouvoir8()
        {
            FormChoixCouleur choixCouleur;
            do
            {
                choixCouleur = new FormChoixCouleur();
                choixCouleur.ShowDialog();
            } while (choixCouleur.Couleur is null);

            return (Carte.Sorte)choixCouleur.Couleur;

        }

    }
}
