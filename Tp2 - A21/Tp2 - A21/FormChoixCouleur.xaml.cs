﻿using System.Windows;

namespace Tp2___A21
{
    /// <summary>
    /// Logique d'interaction pour FormChoixCouleur.xaml
    /// </summary>
    public partial class FormChoixCouleur : Window
    {
        private Carte.Sorte? _couleur = null;

        public Carte.Sorte? Couleur
        {
            get { return _couleur; }
            set { _couleur = value; }
        }


        public FormChoixCouleur()
        {
            InitializeComponent();
            btnChoixCarreau.Width = 75;
            btnChoixCarreau.Height = 75;
            btnChoixPique.Width = 75;
            btnChoixPique.Height = 75;
            btnChoixCoeur.Width = 75;
            btnChoixCoeur.Height = 75;
            btnChoixTrefle.Width = 75;
            btnChoixTrefle.Height = 75;
        }

        private void btnChoixCoeur_Click(object sender, RoutedEventArgs e)
        {
            Couleur = Carte.Sorte.Coeur;
            this.Close();
        }

        private void btnChoixCarreau_Click(object sender, RoutedEventArgs e)
        {
            Couleur = Carte.Sorte.Carreau;
            this.Close();
        }

        private void btnChoixTrefle_Click(object sender, RoutedEventArgs e)
        {
            Couleur = Carte.Sorte.Trèfle;
            this.Close();
        }

        private void btnChoixPique_Click(object sender, RoutedEventArgs e)
        {
            Couleur = Carte.Sorte.Pique;
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Couleur is null)
            {
                e.Cancel = true;
            }

            // TODO : Show error label (no button checked)
        }

        // TODO: Change label color when selected
    }
}
