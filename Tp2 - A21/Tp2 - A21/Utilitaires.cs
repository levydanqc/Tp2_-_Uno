﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Konscious.Security.Cryptography;

namespace Tp2___A21
{
    public static class Utilitaires
    {
        public static Random Aleatoire = new Random();

        /// <summary>
        /// Méthode pour créer un salt d'un mot de passe.
        /// </summary>
        /// <returns>Retourne le salt créé.</returns>
        public static byte[] SaltMotDePasse()
        {
            var buffer = new byte[16];
            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(buffer);
            return buffer;
        }

        /// <summary>
        /// Methode pour hasher un mot de passe.
        /// </summary>
        /// <param name="pPassword">Le mot de passe.</param>
        /// <param name="pSalt">Le salt.</param>
        /// <returns>Le mot de passe avec hashage.</returns>
        public static byte[] HashMotDePasse(string pPassword, byte[] pSalt)
        {
            var argon2 = new Argon2id(Encoding.UTF8.GetBytes(pPassword))
            {
                Salt = pSalt,
                DegreeOfParallelism = 8,
                Iterations = 4,
                MemorySize = 1024 * 1024
            };

            return argon2.GetBytes(16);
        }

        /// <summary>
        /// Methode pour verifier le mot de passe.
        /// </summary>
        /// <param name="pPassword"></param>
        /// <param name="pSalt"></param>
        /// <param name="pHash"></param>
        /// <returns>Valide ou non.</returns>
        public static bool VerifierMdp(string pPassword, byte[] pSalt, byte[] pHash)
        {
            return pHash.SequenceEqual(HashMotDePasse(pPassword, pSalt));
        }
    }
}
